package br.com.iesb.tests;

import br.com.iesb.enums.PlayerTurn;
import br.com.iesb.table.TableState;
import br.com.iesb.table.moves.MovesCore;
import br.com.iesb.helpers.StatesGeneratorHelper;
import br.com.iesb.table.pieces.BluePiece;
import br.com.iesb.table.pieces.RedPiece;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.*;

public class StatesGeneratorHelperTest extends TestCase {

    @Test
    public void testBasicMovesGenerator() {
        int[] coordinates = new int[]{3, 3};

        List<RedPiece> redPieces = Arrays.asList(
                new RedPiece(coordinates[0], coordinates[1], false)
        );

        List<BluePiece> bluePieces = new ArrayList<>(Arrays.asList(
                new BluePiece(4, 2),
                new BluePiece(4, 4),
                new BluePiece(2, 2),
                new BluePiece(2, 4)
        ));

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.RED);

        var possibleMoves = StatesGeneratorHelper.getPossibleMovements(matrix);

        assertTrue(possibleMoves.size() == 4);

        assertTrue(existsState(5, 1, possibleMoves));
        assertTrue(existsState(5, 5, possibleMoves));
        assertTrue(existsState(1, 5, possibleMoves));
        assertTrue(existsState(1, 1, possibleMoves));
        assertTrue(allBlueHasSize(3, possibleMoves));
    }

    @Test
    public void testRecursiveCaptureMovesGenerator() {

        List<RedPiece> redPieces = Arrays.asList(
                new RedPiece(0, 2, false)
        );

        List<BluePiece> bluePieces = new ArrayList<>(Arrays.asList(
                new BluePiece(1, 3),
                new BluePiece(3, 3),
                new BluePiece(3, 5),
                new BluePiece(5, 5)
        ));

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.RED);

        var possibleMoves = StatesGeneratorHelper.getPossibleMovements(matrix);

        var biggestScore = possibleMoves.stream().
                max(Comparator.comparingInt(TableState::getScore)).get();

        assertTrue(possibleMoves.size() == 5);
        assertTrue(existsState(6, 4, possibleMoves));
        assertTrue(existsState(4, 2, possibleMoves));
        assertTrue(existsState(1, 1, possibleMoves));
        assertTrue(biggestScore.getRedPieces().size() == 1);
        assertTrue(biggestScore.getBluePieces().size() == 1);
    }

    @Test
    public void testMoveChecker() {

        List<RedPiece> redPieces = Arrays.asList(
                new RedPiece(0, 2, true)
        );

        List<BluePiece> bluePieces = new ArrayList<>();

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.RED);

        var possibleMoves = StatesGeneratorHelper.getPossibleMovements(matrix);

        var biggestScore = possibleMoves.stream().
                max(Comparator.comparingInt(TableState::getScore)).get();

        assertTrue(possibleMoves.size() == 7);
        assertTrue(biggestScore.getRedPieces().size() == 1);
        assertTrue(biggestScore.getBluePieces().size() == 0);
    }

    @Test
    public void testMoveNonChecker() {

        List<RedPiece> redPieces = Arrays.asList(
                new RedPiece(0, 2, false)
        );

        List<BluePiece> bluePieces = new ArrayList<>();

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.RED);

        var possibleMoves = StatesGeneratorHelper.getPossibleMovements(matrix);

        var biggestScore = possibleMoves.stream().
                max(Comparator.comparingInt(TableState::getScore)).get();

        assertTrue(possibleMoves.size() == 2);
        assertTrue(existsState(1, 1, possibleMoves));
        assertTrue(existsState(1, 3, possibleMoves));
        assertTrue(biggestScore.getRedPieces().size() == 1);
        assertTrue(biggestScore.getBluePieces().size() == 0);
    }

    private boolean existsState(int line, int column, List<TableState> possibleMoves) {
        return possibleMoves.stream().
                anyMatch(p -> p.getRedPieces().
                        stream().
                        anyMatch(p1 -> p1.getLine() == line && p1.getColumn() == column));
    }

    private boolean allBlueHasSize(int maxSize, List<TableState> possibleMoves) {
        return possibleMoves.stream().
                filter(p -> p.getBluePieces().
                        size() == maxSize).count() == possibleMoves.size();
    }
}
