package br.com.iesb.tests;

import br.com.iesb.enums.PlayerTurn;
import br.com.iesb.table.TableState;
import br.com.iesb.table.moves.MovesCore;
import br.com.iesb.table.pieces.Piece;
import br.com.iesb.table.pieces.BluePiece;
import br.com.iesb.table.pieces.RedPiece;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class BlueMinimaxMovesTest extends TestCase {

	@Test
	public void testBlueForwardLeftFalseMoveAgainstUpperBlueEdge() {
		int[] coordinates = new int[]{0,2};

		List<BluePiece> bluePieces = Arrays.asList(
				new BluePiece(coordinates[0], coordinates[1], true)
		);

		List<RedPiece> redPieces = Arrays.asList();

		MovesCore movesCore = new MovesCore(redPieces, bluePieces);

		var matrix = new TableState(movesCore);

		matrix.setPlayerTurn(PlayerTurn.BLUE);

		var possibleMovements = matrix.minimaxForwardLeft(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getBluePiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testBlueForwardRightFalseMoveAgainstUpperBlueEdge() {
		int[] coordinates = new int[]{0,4};

		List<BluePiece> bluePieces = Arrays.asList(
				new BluePiece(coordinates[0], coordinates[1], true)
		);

		List<RedPiece> redPieces = Arrays.asList();

		MovesCore movesCore = new MovesCore(redPieces, bluePieces);

		var matrix = new TableState(movesCore);

		matrix.setPlayerTurn(PlayerTurn.BLUE);

		var possibleMovements = matrix.minimaxForwardRight(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getBluePiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testBlueForwardLeftFalseMoveAgainstLeftEdge() {
		int[] coordinates = new int[]{5, 7};
		var matrix = new TableState();

		matrix.setPlayerTurn(PlayerTurn.BLUE);

		var possibleMovements = matrix.minimaxForwardLeft(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getBluePiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testBlueForwardRightFalseMoveAgainstRightEdge() {
		int[] coordinates = new int[]{6, 0};
		var matrix = new TableState();

		matrix.setPlayerTurn(PlayerTurn.BLUE);

		var possibleMovements = matrix.minimaxForwardRight(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getBluePiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testBlueForwardLeftFalseMoveAgainstBluePiece() {
		int[] coordinates = new int[]{6,2};
		var matrix = new TableState();

		matrix.setPlayerTurn(PlayerTurn.BLUE);

		var possibleMovements = matrix.minimaxForwardLeft(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getBluePiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testBlueForwardRightFalseMoveAgainstBluePiece() {
		int[] coordinates = new int[]{6,4};
		var matrix = new TableState();

		matrix.setPlayerTurn(PlayerTurn.BLUE);

		var possibleMovements = matrix.minimaxForwardRight(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getBluePiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testBlueBackwardLeftFalseMoveAgainstBluePiece() {
		int[] coordinates = new int[]{0,4};

		List<BluePiece> bluePieces = Arrays.asList(
				new BluePiece(coordinates[0], coordinates[1], true),
				new BluePiece(1, 5)
		);

		List<RedPiece> redPieces = Arrays.asList();

		MovesCore movesCore = new MovesCore(redPieces, bluePieces);

		var matrix = new TableState(movesCore);

		matrix.setPlayerTurn(PlayerTurn.BLUE);

		var possibleMovements = matrix.minimaxBackwardLeft(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getBluePiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testBlueBackwardRightFalseMoveAgainstBluePiece() {
		int[] coordinates = new int[]{0,4};

		List<BluePiece> bluePieces = Arrays.asList(
				new BluePiece(coordinates[0], coordinates[1], true),
				new BluePiece(1, 3)
		);

		List<RedPiece> redPieces = Arrays.asList();

		MovesCore movesCore = new MovesCore(redPieces, bluePieces);

		var matrix = new TableState(movesCore);

		matrix.setPlayerTurn(PlayerTurn.BLUE);


		var possibleMovements = matrix.minimaxBackwardRight(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getBluePiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testBlueBackwardLeftFalseMoveAgainstBottomEdge() {
		int[] coordinates = new int[]{7, 3};
		var matrix = new TableState();

		matrix.setPlayerTurn(PlayerTurn.BLUE);

		matrix.getBluePiece(coordinates[0], coordinates[1]).turnoIntoChecker();

		var possibleMovements = matrix.minimaxBackwardLeft(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getBluePiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testBlueBackwardRightFalseMoveAgainstBottomEdge() {
		int[] coordinates = new int[]{7,5};
		var matrix = new TableState();

		matrix.setPlayerTurn(PlayerTurn.BLUE);

		matrix.getBluePiece(coordinates[0], coordinates[1]).turnoIntoChecker();

		var possibleMovements = matrix.minimaxBackwardRight(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getBluePiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testBlueBackwardLeftFalseMoveAgainstLeftEdge() {
		int[] coordinates = new int[]{5, 7};
		var matrix = new TableState();

		matrix.setPlayerTurn(PlayerTurn.BLUE);

		matrix.getBluePiece(coordinates[0], coordinates[1]).turnoIntoChecker();

		var possibleMovements = matrix.minimaxBackwardLeft(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getBluePiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testBlueBackwardRightFalseMoveAgainstRightEdge() {
		int[] coordinates = new int[]{5,7};
		var matrix = new TableState();

		matrix.setPlayerTurn(PlayerTurn.BLUE);

		matrix.getBluePiece(coordinates[0], coordinates[1]).turnoIntoChecker();

		var possibleMovements = matrix.minimaxBackwardRight(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getBluePiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testRedBackwardLeftFalseMoveWithoutBeingChecker() {
		int[] coordinates = new int[]{5, 3};

		List<RedPiece> redPieces = Arrays.asList();

		List<BluePiece> bluePieces = Arrays.asList(
				new BluePiece(coordinates[0], coordinates[1])
		);

		MovesCore movesCore = new MovesCore(redPieces, bluePieces);
		
		var matrix = new TableState(movesCore);

		matrix.setPlayerTurn(PlayerTurn.BLUE);

		matrix.getBluePiece(coordinates[0], coordinates[1]).turnoIntoChecker();

		var possibleMovements = matrix.minimaxBackwardLeft(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getBluePiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testRedBackwardRightFalseMoveWithoutBeingChecker() {
		int[] coordinates = new int[]{5, 3};

		List<RedPiece> redPieces = Arrays.asList(
		);

		List<BluePiece> bluePieces = Arrays.asList(
				new BluePiece(coordinates[0], coordinates[1])
		);

		MovesCore movesCore = new MovesCore(redPieces, bluePieces);

		var matrix = new TableState(movesCore);

		matrix.setPlayerTurn(PlayerTurn.BLUE);

		matrix.getBluePiece(coordinates[0], coordinates[1]).turnoIntoChecker();

		var possibleMovements = matrix.minimaxBackwardRight(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getBluePiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testBlueForwardLeftTrueMove() {

		int[] coordinates = new int[]{5, 3};

		var matrix = new TableState();

		matrix.setPlayerTurn(PlayerTurn.BLUE);

		var possibleMovements = matrix.minimaxForwardLeft(coordinates[0], coordinates[1]);
		matrix = possibleMovements.get(0);

		assertTrue(possibleMovements.size() > 0);
		assertNull(matrix.getBluePiece(coordinates[0], coordinates[1]));
		assertNotNull(matrix.getBluePiece(coordinates[0]-1, coordinates[1]+1));
	}

	@Test
	public void testBlueForwardRightTrueMove() {

		int[] coordinates = new int[]{5, 7};

		var matrix = new TableState();

		matrix.setPlayerTurn(PlayerTurn.BLUE);

		var possibleMovements = matrix.minimaxForwardRight(coordinates[0], coordinates[1]);
		matrix = possibleMovements.get(0);

		assertTrue(possibleMovements.size() > 0);
		assertNull(matrix.getBluePiece(coordinates[0], coordinates[1]));
		assertNotNull(matrix.getBluePiece(coordinates[0]-1, coordinates[1]-1));
	}

	@Test
	public void testBlueBackwardLeftTrueMove() {

		int[] coordinates = new int[]{0,2};

		List<BluePiece> bluePieces = Arrays.asList(
			new BluePiece(coordinates[0], coordinates[1], true)
		);

		List<RedPiece> redPieces = Arrays.asList();

		MovesCore movesCore = new MovesCore(redPieces, bluePieces);

		var matrix = new TableState(movesCore);

		matrix.setPlayerTurn(PlayerTurn.BLUE);


		var possibleMovements = matrix.minimaxBackwardLeft(coordinates[0], coordinates[1]);
		matrix = possibleMovements.get(0);

		assertTrue(possibleMovements.size() > 0);
		assertNotNull(matrix.getBluePiece(coordinates[0]+1, coordinates[1]+1));
	}

	@Test
	public void testBlueBackwardRightTrueMove() {

		int[] coordinates = new int[]{0,2};

		List<BluePiece> bluePieces = Arrays.asList(
				new BluePiece(coordinates[0], coordinates[1], true)
		);

		List<RedPiece> redPieces = Arrays.asList();

		MovesCore movesCore = new MovesCore(redPieces, bluePieces);

		var matrix = new TableState(movesCore);

		matrix.setPlayerTurn(PlayerTurn.BLUE);


		var possibleMovements = matrix.minimaxBackwardRight(coordinates[0], coordinates[1]);
		matrix = possibleMovements.get(0);

		assertTrue(possibleMovements.size() > 0);
		assertNotNull(matrix.getBluePiece(coordinates[0]+1, coordinates[1]-1));
	}

	@Test
	public void testCheckIfBlueTurnedCheckerMovingLeft() {

		int[] coordinates = new int[]{1, 1};

		List<BluePiece> bluePieces = Arrays.asList(
				new BluePiece(coordinates[0], coordinates[1], false)
		);

		List<RedPiece> redPieces = Arrays.asList();

		MovesCore movesCore = new MovesCore(redPieces, bluePieces);

		var matrix = new TableState(movesCore);

		matrix.setPlayerTurn(PlayerTurn.BLUE);


		assertFalse(matrix.getBluePiece(coordinates[0], coordinates[1]).getIsChecker());
		var possibleMovements = matrix.minimaxForwardLeft(coordinates[0], coordinates[1]);
		matrix = possibleMovements.get(0);

		assertTrue(possibleMovements.size() > 0);

		Piece newLocation = matrix.getBluePiece(coordinates[0]-1, coordinates[1]+1);
		assertNotNull(newLocation);
		assertTrue(newLocation.getIsChecker());
	}

	@Test
	public void testCheckIfBlueTurnedCheckerMovingRight() {

		int[] coordinates = new int[]{1, 5};

		List<BluePiece> bluePieces = Arrays.asList(
				new BluePiece(coordinates[0], coordinates[1], false)
		);

		List<RedPiece> redPieces = Arrays.asList();

		MovesCore movesCore = new MovesCore(redPieces, bluePieces);

		var matrix = new TableState(movesCore);

		matrix.setPlayerTurn(PlayerTurn.BLUE);


		assertFalse(matrix.getBluePiece(coordinates[0], coordinates[1]).getIsChecker());
		var possibleMovements = matrix.minimaxForwardRight(coordinates[0], coordinates[1]);
		matrix = possibleMovements.get(0);

		assertTrue(possibleMovements.size() > 0);

		Piece newLocation = matrix.getBluePiece(coordinates[0]-1, coordinates[1]-1);
		assertNotNull(newLocation);
		assertTrue(newLocation.getIsChecker());
	}
}
