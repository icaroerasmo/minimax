package br.com.iesb.tests;

import br.com.iesb.enums.PlayerTurn;
import br.com.iesb.table.TableState;
import br.com.iesb.table.moves.MovesCore;
import br.com.iesb.table.pieces.BluePiece;
import br.com.iesb.table.pieces.RedPiece;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class RedCaptureTest extends TestCase {

    @Test
    public void testForwardLeftCapture() {
        int[] coordinates = new int[]{2, 2};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(coordinates[0], coordinates[1])
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(3, 1)
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.RED);
        
        var possibleMoves = matrix.minimaxForwardLeft(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getBluePieces().size() == 0);
        assertNotNull(matrix.getRedPiece(4, 0));
    }

    @Test
    public void testForwardRightCapture() {
        int[] coordinates = new int[]{1, 5};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(coordinates[0], coordinates[1])
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(2, 6)
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.RED);
        
        var possibleMoves = matrix.minimaxForwardRight(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getBluePieces().size() == 0);
        assertNotNull(matrix.getRedPiece(3, 7));
    }

    @Test
    public void testBackwardLeftCapture() {
        int[] coordinates = new int[]{6, 2};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(coordinates[0], coordinates[1])
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(5, 1)
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.RED);
        
        var possibleMoves = matrix.minimaxBackwardLeft(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getBluePieces().size() == 0);
        assertNotNull(matrix.getRedPiece(4, 0));
    }

    @Test
    public void testBackwardRightCapture() {
        int[] coordinates = new int[]{7, 5};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(coordinates[0], coordinates[1])
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(6, 6)
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.RED);
        
        var possibleMoves = matrix.minimaxBackwardRight(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getBluePieces().size() == 0);
        assertNotNull(matrix.getRedPiece(5, 7));
    }

    @Test
    public void testForwardLeftRecursiveCapture() {
        int[] coordinates = new int[]{0, 6};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                    new RedPiece(coordinates[0], coordinates[1])
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(1, 5),
                        new BluePiece(3, 3),
                        new BluePiece(5, 1)
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.RED);
        
        var possibleMoves = matrix.minimaxForwardLeft(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getBluePieces().size() == 0);
        assertNotNull(matrix.getRedPiece(6, 0));
    }

    @Test
    public void testForwardRightRecursiveCapture() {
        int[] coordinates = new int[]{0, 0};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(coordinates[0], coordinates[1])
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(1, 1),
                        new BluePiece(3, 3),
                        new BluePiece(5, 5)
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.RED);
        
        var possibleMoves = matrix.minimaxForwardRight(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getBluePieces().size() == 0);
        assertNotNull(matrix.getRedPiece(6, 6));
    }

    @Test
    public void testBackwardLeftRecursiveCapture() {
        int[] coordinates = new int[]{7, 7};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(coordinates[0], coordinates[1])
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(6, 6),
                        new BluePiece(4, 4),
                        new BluePiece(2, 2)
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.RED);
        
        var possibleMoves = matrix.minimaxBackwardLeft(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getBluePieces().size() == 0);
        assertNotNull(matrix.getRedPiece(1, 1));
    }

    @Test
    public void testBackwardRightRecursiveCapture() {
        int[] coordinates = new int[]{7, 1};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(coordinates[0], coordinates[1])
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(6, 2),
                        new BluePiece(4, 4),
                        new BluePiece(2, 6)
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.RED);
        
        var possibleMoves = matrix.minimaxBackwardRight(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getBluePieces().size() == 0);
        assertNotNull(matrix.getRedPiece(1, 7));
    }

    @Test
    public void testZigzagFromForwardLeftRecursiveCapture() {
        int[] coordinates = new int[]{0, 4};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(coordinates[0], coordinates[1])
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(1, 3),
                        new BluePiece(3, 3),
                        new BluePiece(5, 3)
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.RED);
        
        var possibleMoves = matrix.minimaxForwardLeft(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getBluePieces().size() == 0);
        assertNotNull(matrix.getRedPiece(6, 2));
    }

    @Test
    public void testZigzagFromForwardRightRecursiveCapture() {
        int[] coordinates = new int[]{0, 4};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(coordinates[0], coordinates[1])
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(1, 5),
                        new BluePiece(3, 5),
                        new BluePiece(5, 5)
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.RED);
        
        var possibleMoves = matrix.minimaxForwardRight(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getBluePieces().size() == 0);
        assertNotNull(matrix.getRedPiece(6, 6));
    }

    @Test
    public void testZigzagFromBackwardLeftRecursiveCapture() {
        int[] coordinates = new int[]{6, 6};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(coordinates[0], coordinates[1])
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(1, 5),
                        new BluePiece(3, 5),
                        new BluePiece(5, 5)
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.RED);
        
        var possibleMoves = matrix.minimaxBackwardLeft(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getBluePieces().size() == 0);
        assertNotNull(matrix.getRedPiece(0, 4));
    }

    @Test
    public void testZigzagFromBackwardRightRecursiveCapture() {
        int[] coordinates = new int[]{6, 2};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(coordinates[0], coordinates[1])
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(1, 3),
                        new BluePiece(3, 3),
                        new BluePiece(5, 3)
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.RED);
        
        var possibleMoves = matrix.minimaxBackwardRight(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getBluePieces().size() == 0);
        assertNotNull(matrix.getRedPiece(0, 4));
    }

    @Test
    public void testSquareShapeFromForwardLeftRecursiveCapture() {
        int[] coordinates = new int[]{1, 3};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(coordinates[0], coordinates[1])
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(4, 2),
                        new BluePiece(4, 4),
                        new BluePiece(2, 4),
                        new BluePiece(2, 2)
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.RED);
        
        var possibleMoves = matrix.minimaxForwardLeft(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getBluePieces().size() == 0);
        assertNotNull(matrix.getRedPiece(coordinates[0], coordinates[1]));
    }
    @Test
    public void testSquareShapeFromForwardRightRecursiveCapture() {
        int[] coordinates = new int[]{1, 3};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(coordinates[0], coordinates[1])
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(4, 2),
                        new BluePiece(4, 4),
                        new BluePiece(2, 4),
                        new BluePiece(2, 2)
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.RED);
        
        var possibleMoves = matrix.minimaxForwardRight(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getBluePieces().size() == 0);
        assertNotNull(matrix.getRedPiece(coordinates[0], coordinates[1]));
    }

    @Test
    public void testSquareShapeFromBackwardLeftRecursiveCapture() {
        int[] coordinates = new int[]{5, 3};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(coordinates[0], coordinates[1])
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(4, 2),
                        new BluePiece(4, 4),
                        new BluePiece(2, 4),
                        new BluePiece(2, 2)
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.RED);
        
        var possibleMoves = matrix.minimaxBackwardLeft(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getBluePieces().size() == 0);
        assertNotNull(matrix.getRedPiece(coordinates[0], coordinates[1]));
    }
    @Test
    public void testSquareShapeFromBackwardRightRecursiveCapture() {
        int[] coordinates = new int[]{5, 3};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(coordinates[0], coordinates[1])
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(4, 2),
                        new BluePiece(4, 4),
                        new BluePiece(2, 4),
                        new BluePiece(2, 2)
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.RED);
        
        var possibleMoves = matrix.minimaxBackwardRight(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getBluePieces().size() == 0);
        assertNotNull(matrix.getRedPiece(coordinates[0], coordinates[1]));
    }
}
