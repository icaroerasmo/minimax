package br.com.iesb.tests;

import br.com.iesb.enums.PlayerTurn;
import br.com.iesb.table.TableState;
import br.com.iesb.table.moves.MovesCore;
import br.com.iesb.table.pieces.BluePiece;
import br.com.iesb.table.pieces.RedPiece;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class BlueCaptureTest extends TestCase {

    @Test
    public void testForwardLeftCapture() {
        int[] coordinates = new int[]{4, 5};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(3, 6)
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(coordinates[0], coordinates[1])
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.BLUE);

        var possibleMoves = matrix.minimaxForwardLeft(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getRedPieces().size() == 0);
        assertNotNull(matrix.getBluePiece(2, 7));
    }

    @Test
    public void testForwardRightCapture() {
        int[] coordinates = new int[]{4, 2};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(3, 1)
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(coordinates[0], coordinates[1])
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.BLUE);

        var possibleMoves = matrix.minimaxForwardRight(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getRedPieces().size() == 0);
        assertNotNull(matrix.getBluePiece(2, 0));
    }

    @Test
    public void testBackwardLeftCapture() {
        int[] coordinates = new int[]{1, 5};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(2, 6)
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(coordinates[0], coordinates[1])
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.BLUE);

        var possibleMoves = matrix.minimaxBackwardLeft(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getRedPieces().size() == 0);
        assertNotNull(matrix.getBluePiece(3, 7));
    }

    @Test
    public void testBackwardRightCapture() {
        int[] coordinates = new int[]{0, 2};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(1, 1)
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(coordinates[0], coordinates[1])
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.BLUE);

        var possibleMoves = matrix.minimaxBackwardRight(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getRedPieces().size() == 0);
        assertNotNull(matrix.getBluePiece(2, 0));
    }

    @Test
    public void testForwardLeftRecursiveCapture() {
        int[] coordinates = new int[]{7, 1};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(6, 2),
                        new RedPiece(4, 4),
                        new RedPiece(2, 6)
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(coordinates[0], coordinates[1])
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);
        

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.BLUE);

        var possibleMoves = matrix.minimaxForwardLeft(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getRedPieces().size() == 0);
        assertNotNull(matrix.getBluePiece(1, 7));
    }

    @Test
    public void testForwardRightRecursiveCapture() {
        int[] coordinates = new int[]{7, 7};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(6, 6),
                        new RedPiece(4, 4),
                        new RedPiece(2, 2)
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(coordinates[0], coordinates[1])
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.BLUE);

        var possibleMoves = matrix.minimaxForwardRight(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getRedPieces().size() == 0);
        assertNotNull(matrix.getBluePiece(1, 1));
    }

    @Test
    public void testBackwardLeftRecursiveCapture() {
        int[] coordinates = new int[]{0, 0};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                    new RedPiece(1, 1),
                    new RedPiece(3, 3),
                    new RedPiece(5, 5)
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(coordinates[0], coordinates[1])
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.BLUE);

        var possibleMoves = matrix.minimaxBackwardLeft(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getRedPieces().size() == 0);
        assertNotNull(matrix.getBluePiece(6, 6));
    }

    @Test
    public void testBackwardRightRecursiveCapture() {
        int[] coordinates = new int[]{0, 6};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                    new RedPiece(1, 5),
                    new RedPiece(3, 3),
                    new RedPiece(5, 1)
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(coordinates[0], coordinates[1])
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.BLUE);

        var possibleMoves = matrix.minimaxBackwardRight(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getRedPieces().size() == 0);
        assertNotNull(matrix.getBluePiece(6, 0));
    }

    @Test
    public void testZigzagFromForwardLeftRecursiveCapture() {
        int[] coordinates = new int[]{7, 1};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(6, 2),
                        new RedPiece(4, 2),
                        new RedPiece(2, 2)
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(coordinates[0], coordinates[1])
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.BLUE);

        var possibleMoves = matrix.minimaxForwardLeft(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getRedPieces().size() == 0);
        assertNotNull(matrix.getBluePiece(1, 3));
    }

    @Test
    public void testZigzagFromForwardRightRecursiveCapture() {
        int[] coordinates = new int[]{7, 7};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(6, 6),
                        new RedPiece(4, 6),
                        new RedPiece(2, 6)
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(coordinates[0], coordinates[1])
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.BLUE);

        var possibleMoves = matrix.minimaxForwardRight(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getRedPieces().size() == 0);
        assertNotNull(matrix.getBluePiece(1, 5));
    }

    @Test
    public void testZigzagFromBackwardLeftRecursiveCapture() {
        int[] coordinates = new int[]{0, 2};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(1, 3),
                        new RedPiece(3, 3),
                        new RedPiece(5, 3)
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(coordinates[0], coordinates[1])
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.BLUE);

        var possibleMoves = matrix.minimaxBackwardLeft(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getRedPieces().size() == 0);
        assertNotNull(matrix.getBluePiece(6, 4));
    }

    @Test
    public void testZigzagFromBackwardRightRecursiveCapture() {
        int[] coordinates = new int[]{0, 6};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(1, 5),
                        new RedPiece(3, 5),
                        new RedPiece(5, 5)
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(coordinates[0], coordinates[1])
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.BLUE);

        var possibleMoves = matrix.minimaxBackwardRight(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getRedPieces().size() == 0);
        assertNotNull(matrix.getBluePiece(6, 4));
    }

    @Test
    public void testSquareShapeFromForwardLeftRecursiveCapture() {
        int[] coordinates = new int[]{5, 3};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(4, 2),
                        new RedPiece(4, 4),
                        new RedPiece(2, 4),
                        new RedPiece(2, 2)
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(coordinates[0], coordinates[1])
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.BLUE);

        var possibleMoves = matrix.minimaxForwardLeft(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getRedPieces().size() == 0);
        assertNotNull(matrix.getBluePiece(coordinates[0], coordinates[1]));
    }
    @Test
    public void testSquareShapeFromForwardRightRecursiveCapture() {
        int[] coordinates = new int[]{5, 3};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(4, 2),
                        new RedPiece(4, 4),
                        new RedPiece(2, 4),
                        new RedPiece(2, 2)
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(coordinates[0], coordinates[1])
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.BLUE);

        var possibleMoves = matrix.minimaxForwardRight(coordinates[0], coordinates[1]);
        matrix = matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getRedPieces().size() == 0);
        assertNotNull(matrix.getBluePiece(coordinates[0], coordinates[1]));
    }

    @Test
    public void testSquareShapeFromBackwardLeftRecursiveCapture() {
        int[] coordinates = new int[]{1, 3};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(4, 2),
                        new RedPiece(4, 4),
                        new RedPiece(2, 4),
                        new RedPiece(2, 2)
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(coordinates[0], coordinates[1])
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.BLUE);

        var possibleMoves = matrix.minimaxBackwardLeft(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getRedPieces().size() == 0);
        assertNotNull(matrix.getBluePiece(coordinates[0], coordinates[1]));
    }
    @Test
    public void testSquareShapeFromBackwardRightRecursiveCapture() {
        int[] coordinates = new int[]{1, 3};

        List<RedPiece> redPieces = new ArrayList(
                Arrays.asList(
                        new RedPiece(4, 2),
                        new RedPiece(4, 4),
                        new RedPiece(2, 4),
                        new RedPiece(2, 2)
                )
        );

        List<BluePiece> bluePieces = new ArrayList(
                Arrays.asList(
                        new BluePiece(coordinates[0], coordinates[1])
                )
        );

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        

        var matrix = new TableState(movesCore);

        matrix.setPlayerTurn(PlayerTurn.BLUE);

        var possibleMoves = matrix.minimaxBackwardRight(coordinates[0], coordinates[1]);
        matrix = possibleMoves.stream().max(Comparator.comparingInt(TableState::getScore)).orElse(null);
        assertTrue(possibleMoves.size() > 0);
        assertTrue(matrix.getRedPieces().size() == 0);
        assertNotNull(matrix.getBluePiece(coordinates[0], coordinates[1]));
    }
}
