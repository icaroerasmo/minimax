package br.com.iesb.tests;

import br.com.iesb.enums.PlayerTurn;
import br.com.iesb.table.TableState;
import br.com.iesb.table.moves.MovesCore;
import br.com.iesb.table.pieces.BluePiece;
import br.com.iesb.table.pieces.RedPiece;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserMovesTests {

    @Test
    public void testGetPossibleMoves() {
        List<RedPiece> redPieces = Arrays.asList(
                new RedPiece(0, 2, true),
                new RedPiece(0, 4, true)
        );

        List<BluePiece> bluePieces = new ArrayList<>(Arrays.asList(
                new BluePiece(1, 3),
                new BluePiece(3, 3),
                new BluePiece(3, 5),
                new BluePiece(5, 5)
        ));

        MovesCore movesCore = new MovesCore(redPieces, bluePieces);

        var matrix = new TableState(movesCore);
        matrix.setPlayerTurn(PlayerTurn.RED);
        var list = matrix.getPossibleMovesForUser(0, 2);
        Assert.assertTrue(list.size() == 6);
    }
}
