package br.com.iesb.tests;

import br.com.iesb.enums.PlayerTurn;
import br.com.iesb.table.TableState;
import br.com.iesb.table.moves.MovesCore;
import br.com.iesb.table.pieces.BluePiece;
import br.com.iesb.table.pieces.Piece;
import br.com.iesb.table.pieces.RedPiece;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class RedMinimaxMovesTest extends TestCase {

	@Test
	public void testRedForwardLeftFalseMoveAgainstUpperEdge() {
		int[] coordinates = new int[]{7, 1};

		List<RedPiece> redPieces = Arrays.asList(
				new RedPiece(coordinates[0], coordinates[1], true)
		);

		List<BluePiece> bluePieces = Arrays.asList();

		MovesCore movesCore = new MovesCore(redPieces, bluePieces);

		var matrix = new TableState(movesCore);

		matrix.setPlayerTurn(PlayerTurn.RED);

		var possibleMovements = matrix.minimaxForwardLeft(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getRedPiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testRedForwardRightFalseMoveAgainstUpperEdge() {
		int[] coordinates = new int[]{7, 5};

		List<RedPiece> redPieces = Arrays.asList(
				new RedPiece(coordinates[0], coordinates[1], true)
		);

		List<BluePiece> bluePieces = Arrays.asList();

		MovesCore movesCore = new MovesCore(redPieces, bluePieces);

		var matrix = new TableState(movesCore);

		matrix.setPlayerTurn(PlayerTurn.RED);

		var possibleMovements = matrix.minimaxForwardRight(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getRedPiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testRedForwardLeftFalseMoveAgainstLeftEdge() {
		int[] coordinates = new int[]{0, 2};
		var matrix = new TableState();

		matrix.setPlayerTurn(PlayerTurn.RED);

		var possibleMovements = matrix.minimaxForwardLeft(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getRedPiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testRedForwardRightFalseMoveAgainstRightEdge() {
		int[] coordinates = new int[]{1, 7};
		var matrix = new TableState();

		matrix.setPlayerTurn(PlayerTurn.RED);

		var possibleMovements = matrix.minimaxForwardRight(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getRedPiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testRedForwardLeftFalseMoveAgainstRedPiece() {
		int[] coordinates = new int[]{1, 1};
		var matrix = new TableState();

		matrix.setPlayerTurn(PlayerTurn.RED);

		var possibleMovements = matrix.minimaxForwardLeft(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getRedPiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testRedForwardRightFalseMoveAgainstRedPiece() {
		int[] coordinates = new int[]{1, 5};
		var matrix = new TableState();

		matrix.setPlayerTurn(PlayerTurn.RED);

		var possibleMovements = matrix.minimaxForwardRight(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getRedPiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testRedBackwardLeftFalseMoveAgainstRedPiece() {
		int[] coordinates = new int[]{7, 5};

		List<RedPiece> redPieces = Arrays.asList(
				new RedPiece(coordinates[0], coordinates[1], true),
				new RedPiece(6, 4)
		);

		List<BluePiece> bluePieces = Arrays.asList();

		MovesCore movesCore = new MovesCore(redPieces, bluePieces);

		var matrix = new TableState(movesCore);

		matrix.setPlayerTurn(PlayerTurn.RED);

		var possibleMovements = matrix.minimaxBackwardLeft(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getRedPiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testRedBackwardRightFalseMoveAgainstRedPiece() {
		int[] coordinates = new int[]{7, 5};

		List<RedPiece> redPieces = Arrays.asList(
				new RedPiece(coordinates[0], coordinates[1], true),
				new RedPiece(6, 6)
		);

		List<BluePiece> bluePieces = Arrays.asList();

		MovesCore movesCore = new MovesCore(redPieces, bluePieces);

		var matrix = new TableState(movesCore);

		matrix.setPlayerTurn(PlayerTurn.RED);

		var possibleMovements = matrix.minimaxBackwardRight(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getRedPiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testRedBackwardLeftFalseMoveAgainstBottomEdge() {
		int[] coordinates = new int[]{0, 2};
		var matrix = new TableState();

		matrix.setPlayerTurn(PlayerTurn.RED);

		matrix.getRedPiece(coordinates[0], coordinates[1]).turnoIntoChecker();

		var possibleMovements = matrix.minimaxBackwardLeft(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getRedPiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testRedBackwardRightFalseMoveAgainstBottomEdge() {
		int[] coordinates = new int[]{0, 2};
		var matrix = new TableState();

		matrix.setPlayerTurn(PlayerTurn.RED);

		matrix.getRedPiece(coordinates[0], coordinates[1]).turnoIntoChecker();

		var possibleMovements = matrix.minimaxBackwardRight(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getRedPiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testRedBackwardLeftFalseMoveAgainstLeftEdge() {
		int[] coordinates = new int[]{2, 0};
		var matrix = new TableState();

		matrix.setPlayerTurn(PlayerTurn.RED);

		matrix.getRedPiece(coordinates[0], coordinates[1]).turnoIntoChecker();

		var possibleMovements = matrix.minimaxBackwardLeft(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getRedPiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testRedBackwardRightFalseMoveAgainstRightEdge() {
		int[] coordinates = new int[]{1, 7};
		var matrix = new TableState();

		matrix.setPlayerTurn(PlayerTurn.RED);

		matrix.getRedPiece(coordinates[0], coordinates[1]).turnoIntoChecker();

		var possibleMovements = matrix.minimaxBackwardRight(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getRedPiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testRedBackwardLeftFalseMoveWithoutBeingChecker() {
		int[] coordinates = new int[]{5, 3};

		List<RedPiece> redPieces = Arrays.asList(
				new RedPiece(coordinates[0], coordinates[1])
		);

		List<BluePiece> bluePieces = Arrays.asList();

		MovesCore movesCore = new MovesCore(redPieces, bluePieces);
		

		var matrix = new TableState(movesCore);

		matrix.setPlayerTurn(PlayerTurn.RED);

		matrix.getRedPiece(coordinates[0], coordinates[1]).turnoIntoChecker();

		var possibleMovements = matrix.minimaxBackwardLeft(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getRedPiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testRedBackwardRightFalseMoveWithoutBeingChecker() {
		int[] coordinates = new int[]{5, 3};

		List<RedPiece> redPieces = Arrays.asList(
				new RedPiece(coordinates[0], coordinates[1])
		);

		List<BluePiece> bluePieces = Arrays.asList();

		MovesCore movesCore = new MovesCore(redPieces, bluePieces);

		var matrix = new TableState(movesCore);

		matrix.setPlayerTurn(PlayerTurn.RED);

		matrix.getRedPiece(coordinates[0], coordinates[1]).turnoIntoChecker();

		var possibleMovements = matrix.minimaxBackwardRight(coordinates[0], coordinates[1]);

		assertTrue(possibleMovements.size() < 1);
		assertNotNull(matrix.getRedPiece(coordinates[0], coordinates[1]));
	}

	@Test
	public void testRedForwardLeftTrueMove() {

		int[] coordinates = new int[]{2, 2};

		var matrix = new TableState();

		matrix.setPlayerTurn(PlayerTurn.RED);

		var possibleMovements = matrix.minimaxForwardLeft(coordinates[0], coordinates[1]);
		matrix = possibleMovements.get(0);

		assertTrue(possibleMovements.size() > 0);
		assertNull(matrix.getRedPiece(coordinates[0], coordinates[1]));
		assertNotNull(matrix.getRedPiece(coordinates[0]+1, coordinates[1]-1));
	}

	@Test
	public void testRedForwardRightTrueMove() {

		int[] coordinates = new int[]{2, 0};

		var matrix = new TableState();

		matrix.setPlayerTurn(PlayerTurn.RED);

		var possibleMovements = matrix.minimaxForwardRight(coordinates[0], coordinates[1]);
		matrix = possibleMovements.get(0);

		assertTrue(possibleMovements.size() > 0);
		assertNull(matrix.getRedPiece(coordinates[0], coordinates[1]));
		assertNotNull(matrix.getRedPiece(coordinates[0]+1, coordinates[1]+1));
	}

	@Test
	public void testRedBackwardLeftTrueMove() {

		int[] coordinates = new int[]{7, 1};

		List<RedPiece> redPieces = Arrays.asList(
			new RedPiece(coordinates[0], coordinates[1], true)
		);

		List<BluePiece> bluePieces = Arrays.asList();

		MovesCore movesCore = new MovesCore(redPieces, bluePieces);

		var matrix = new TableState(movesCore);

		matrix.setPlayerTurn(PlayerTurn.RED);

		var possibleMovements = matrix.minimaxBackwardLeft(coordinates[0], coordinates[1]);
		matrix = possibleMovements.get(0);

		assertTrue(possibleMovements.size() > 0);
		assertNotNull(matrix.getRedPiece(coordinates[0]-1, coordinates[1]-1));
	}

	@Test
	public void testRedBackwardRightTrueMove() {

		int[] coordinates = new int[]{7, 1};

		List<RedPiece> redPieces = Arrays.asList(
				new RedPiece(coordinates[0], coordinates[1], true)
		);

		List<BluePiece> bluePieces = Arrays.asList();

		MovesCore movesCore = new MovesCore(redPieces, bluePieces);

		var matrix = new TableState(movesCore);

		matrix.setPlayerTurn(PlayerTurn.RED);

		var possibleMovements = matrix.minimaxBackwardRight(coordinates[0], coordinates[1]);
		matrix = possibleMovements.get(0);

		assertTrue(possibleMovements.size() > 0);
		assertNotNull(matrix.getRedPiece(coordinates[0]-1, coordinates[1]+1));
	}

	@Test
	public void testCheckIfRedTurnedCheckerMovingLeft() {

		int[] coordinates = new int[]{6, 6};

		List<RedPiece> redPieces = Arrays.asList(
				new RedPiece(coordinates[0], coordinates[1], false)
		);

		List<BluePiece> bluePieces = Arrays.asList();

		MovesCore movesCore = new MovesCore(redPieces, bluePieces);
		
		var matrix = new TableState(movesCore);

		matrix.setPlayerTurn(PlayerTurn.RED);

		assertFalse(matrix.getRedPiece(coordinates[0], coordinates[1]).getIsChecker());

		var possibleMovements = matrix.minimaxForwardLeft(coordinates[0], coordinates[1]);
		matrix = possibleMovements.get(0);

		assertTrue(possibleMovements.size() > 0);

		Piece newLocation = matrix.getRedPiece(coordinates[0]+1, coordinates[1]-1);
		assertNotNull(newLocation);
		assertTrue(newLocation.getIsChecker());
	}

	@Test
	public void testCheckIfRedTurnedCheckerMovingRight() {

		int[] coordinates = new int[]{6, 2};

		List<RedPiece> redPieces = Arrays.asList(
				new RedPiece(coordinates[0], coordinates[1], false)
		);

		List<BluePiece> bluePieces = Arrays.asList();

		MovesCore movesCore = new MovesCore(redPieces, bluePieces);
		
		var matrix = new TableState(movesCore);

		matrix.setPlayerTurn(PlayerTurn.RED);

		assertFalse(matrix.getRedPiece(coordinates[0], coordinates[1]).getIsChecker());

		var possibleMovements = matrix.minimaxForwardRight(coordinates[0], coordinates[1]);
		matrix = possibleMovements.get(0);

		assertTrue(possibleMovements.size() > 0);

		Piece newLocation = matrix.getRedPiece(coordinates[0]+1, coordinates[1]+1);
		assertNotNull(newLocation);
		assertTrue(newLocation.getIsChecker());
	}
}
