package br.com.iesb.table.moves;

import br.com.iesb.enums.Direction;
import br.com.iesb.enums.MoveType;
import br.com.iesb.table.TableState;
import br.com.iesb.table.moves.model.MovesLog;
import br.com.iesb.table.pieces.Piece;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.*;

/**
 * MinimaxMoves
 */
public class MinimaxMoves extends MovesAbstract {

    public MinimaxMoves(MovesCore moves) {
        super(moves);
    }

    public List<TableState> moveForwardLeft(Piece piece, TableState origin) {
        List<TableState> matrixes = new ArrayList<>();
        move(piece, origin, Direction.FORWARD_LEFT, matrixes);
        return matrixes;
    }
    
    public List<TableState> moveForwardRight(Piece piece, TableState origin) {
        List<TableState> matrixes = new ArrayList<>();
        move(piece, origin, Direction.FORWARD_RIGHT, matrixes);
        return matrixes;
    }
    
    public List<TableState> moveBackwardLeft(Piece piece, TableState origin) {
        List<TableState> matrixes = new ArrayList<>();
        move(piece, origin, Direction.BACKWARD_LEFT, matrixes);
        return matrixes;
    }
    
    public List<TableState> moveBackwardRight(Piece piece, TableState origin)  {
        List<TableState> matrixes = new ArrayList<>();
        move(piece, origin, Direction.BACKWARD_RIGHT, matrixes);
        return matrixes;
    }

    public void move(Piece piece, TableState origin, Direction direction, List<TableState> matrixes) {
        int[] coordinates;

        switch(direction) {
            case FORWARD_LEFT -> coordinates = piece.testMoveForwardLeft();
            case FORWARD_RIGHT -> coordinates = piece.testMoveForwardRight();
            case BACKWARD_LEFT -> coordinates = piece.testMoveBackwardLeft();
            case BACKWARD_RIGHT -> coordinates = piece.testMoveBackwardRight();
            default -> throw new RuntimeException("Illegal value");
        }

        if (origin.theresPlayer(coordinates) ||
                !isMovementPermitted(coordinates) ||
                origin.theresPlayer(coordinates)) {
            return;
        }

        if (theresOpponent(coordinates)) {

            var newState = origin.getCopy();
            var newPiece = newState.getPlayerPiece(piece.getLine(), piece.getColumn());

            List<TableState> newMatrix = capture(newPiece, newState, direction);

            if(newMatrix != null) {
                matrixes.addAll(newMatrix);
            }
        } else if(piece.getIsChecker() || !direction.getMustBeChecker()) {

            int[] from = new int[]{piece.getLine(), piece.getColumn()};

            switch(direction) {
                case FORWARD_LEFT -> piece.moveForwardLeft();
                case FORWARD_RIGHT -> piece.moveForwardRight();
                case BACKWARD_LEFT -> piece.moveBackwardLeft();
                case BACKWARD_RIGHT -> piece.moveBackwardRight();
                default -> throw new RuntimeException("Illegal value");
            }

            int[] to = new int[]{piece.getLine(), piece.getColumn()};

            origin.addMessage(new MovesLog(from, to, origin.getPlayerTurn(), MoveType.MOVE, direction));

            matrixes.add(origin);

            var newState = origin.getCopy();
            var newPiece = newState.getPlayerPiece(piece.getLine(), piece.getColumn());

            if(piece.getIsChecker()) {
                move(newPiece, newState, direction, matrixes);
            }
        }
    }
    
    public List<TableState> multiDirectionCapture(Piece piece, TableState origin) {

        List<TableState> matrixes = new ArrayList<>();

        if (origin.theresOpponentForwardLeft(piece)) {
            var newState = origin.getCopy();
            var newPiece = newState.getPlayerPiece(piece.getLine(), piece.getColumn());
            var newTable = capture(newPiece, newState, Direction.FORWARD_LEFT);

            if(newTable != null) {
                matrixes.addAll(newTable);
            }
        }
        if (origin.theresOpponentForwardRight(piece)) {
            var newState = origin.getCopy();
            var newPiece = newState.getPlayerPiece(piece.getLine(), piece.getColumn());
            var newTable = capture(newPiece, newState, Direction.FORWARD_RIGHT);

            if(newTable != null) {
                matrixes.addAll(newTable);
            }
        }
        if (origin.theresOpponentBackwardLeft(piece)) {
            var newState = origin.getCopy();
            var newPiece = newState.getPlayerPiece(piece.getLine(), piece.getColumn());
            var newTable = capture(newPiece, newState, Direction.BACKWARD_LEFT);

            if(newTable != null) {
                matrixes.addAll(newTable);
            }
        }
        if (origin.theresOpponentBackwardRight(piece)) {
            var newState = origin.getCopy();
            var newPiece = newState.getPlayerPiece(piece.getLine(), piece.getColumn());
            var newTable = capture(newPiece, newState, Direction.BACKWARD_RIGHT);

            if(newTable != null) {
                matrixes.addAll(newTable);
            }
        }

        return matrixes;
    }
    
    public List<TableState> capture(Piece piece, TableState origin, Direction direction) {

        if (!captured(piece, origin, direction)){
            return null;
        }

        List<TableState> matrixes = new ArrayList<>();

        matrixes.add(origin);

        var newMatrix = multiDirectionCapture(piece, origin);

        if(newMatrix != null) {
            matrixes.addAll(newMatrix);
        }

        return matrixes;
    }

    
    public int hashCode() {
        return new HashCodeBuilder(
                17,
                37).
                append(getMoves()).toHashCode();
    }

    
    public boolean equals(Object obj) {

        if(obj == null || !(obj instanceof MinimaxMoves)) {
            return false;
        }

        MinimaxMoves basicMoves = (MinimaxMoves) obj;

        return new EqualsBuilder().
                append(getMoves(), basicMoves.getMoves()).
                isEquals();
    }
}
