package br.com.iesb.table.moves.model;

import br.com.iesb.enums.Direction;
import br.com.iesb.enums.MoveType;
import br.com.iesb.enums.PlayerTurn;

/**
 * MovesLog
 */
public class MovesLog {
    private int[] from;
    private int[] captured;
    private int[] to;
    private PlayerTurn playerTurn;
    private MoveType moveType;
    private Direction direction;
    private boolean isRead = false;

    public MovesLog(
            int[] from, int[] to,
            PlayerTurn playerTurn, MoveType moveType,
            Direction direction) {
        this.from = from;
        this.to = to;
        this.playerTurn = playerTurn;
        this.moveType = moveType;
        this.direction = direction;
    }

    public MovesLog(
            int[] from, int[] captured, int[] to,
            PlayerTurn playerTurn, MoveType moveType,
            Direction direction) {
        this.from = from;
        this.to = to;
        this.playerTurn = playerTurn;
        this.captured = captured;
        this.moveType = moveType;
        this.direction = direction;
    }

    public int[] getFrom() {
        return from;
    }

    public int[] getCaptured() {
        return captured;
    }

    public int[] getTo() {
        return to;
    }

    public PlayerTurn getPlayerTurn() {
        return playerTurn;
    }

    public MoveType getMoveType() {
        return moveType;
    }

    public Direction getDirection() {
        return direction;
    }

    public boolean isRead() {
        return isRead;
    }

    public boolean isNotRead() {
        return !isRead();
    }

    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder();

        builder.append(String.format("%s in [%s,%s] %s %s", playerTurn, from[0], from[1],  moveType, direction));

        switch(moveType) {
            case MOVE:
                builder.append(String.format(" to [%s,%s]", to[0], to[1]));
                break;
            case CAPTURE:
                builder.append(String.format(" [%s,%s] and now is in [%s,%s]", captured[0], captured[1], to[0], to[1]));
                break;
        }

        isRead = true;

        return builder.toString();
    }
}
