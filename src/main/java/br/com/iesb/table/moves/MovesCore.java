package br.com.iesb.table.moves;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import br.com.iesb.enums.PlayerTurn;
import br.com.iesb.table.pieces.BluePiece;
import br.com.iesb.table.pieces.Piece;
import br.com.iesb.table.pieces.RedPiece;

/**
 * MovesCore
 */
public class MovesCore {

    private List<RedPiece> redPieces;
    private List<BluePiece> bluePieces;

    private PlayerTurn playerTurn = PlayerTurn.RED;

    public MovesCore(
        List<RedPiece> redPieces,
        List<BluePiece> bluePieces
        ) {
            this.redPieces = redPieces;
            this.bluePieces = bluePieces;
    }
    
    public MovesCore(
            List<RedPiece> redPieces,
            List<BluePiece> bluePieces,
            PlayerTurn playerTurn
            ) {
                this(redPieces, bluePieces);
                this.playerTurn = playerTurn;
    }

    public MovesCore() {
        addRedPieces();
        addBluePieces();
    }

    public void changePlayersTurn() {
        playerTurn = playerTurn.changeTurn();
    }

    public boolean isRedTurn() {
        return playerTurn.equals(PlayerTurn.RED);
    }

    public boolean isBlueTurn() {
        return playerTurn.equals(PlayerTurn.BLUE);
    }
    
    public List<RedPiece> getRedPieces() {
    	return redPieces;
    }
    
    public List<BluePiece> getBluePieces() {
    	return bluePieces;
    }

    public List<? extends Piece> getPlayerPieces() {
        return isRedTurn() ? getRedPieces() : getBluePieces();
    }

    public List<? extends Piece> getOpponentPieces() {
        return isRedTurn() ? getBluePieces() : getRedPieces();
    }

    public Piece getPlayerPiece(int line, int column) {
        return getPiece(getPlayerPieces(), line, column);
    }

    public Piece getOpponentPiece(int line, int column) {
        return getPiece(getOpponentPieces(), line, column);
    }

    Piece getPiece(List<? extends Piece> pieces, int line, int column) {
        return pieces.stream().filter(
            p -> p.getLine() == line &&
             p.getColumn() == column
             ).findFirst().orElse(null);
    }

    public RedPiece getRedPiece(int line, int column) {
        return (RedPiece) getPiece(redPieces, line, column);
    }

    public BluePiece getBluePiece(int line, int column) {
        return (BluePiece) getPiece(bluePieces, line, column);
    }

    private void remove(List< ? extends Piece> pieces, int line, int column) {
        pieces.removeIf(p->p.getLine() == line && p.getColumn() == column);
    }

    public void removeOpponentPiece(int line, int column) {
        remove(getOpponentPieces(), line, column);
    }

    private void addRedPieces() {
		List<RedPiece> reds = new ArrayList<>();
        for(int line = 0; line < 3; line++) {
            for(int column = 7; column > -1; column--) {
                if ((line+column) % 2 == 0) {
                    RedPiece red = new RedPiece(line, column);
                    reds.add(red);
                }
            }
        }
        this.redPieces = reds;
    }
    
    private void addBluePieces() {
		List<BluePiece> blues = new ArrayList<>();
        for(int line = 5; line < 8; line++) {
            for(int column = 0; column < 8; column++) {
                if ((line+column) % 2 == 0) {
                    BluePiece blue = new BluePiece(line, column);
                    blues.add(blue);
                }
            }
        }
        this.bluePieces = blues;
    }

    public PlayerTurn getPlayerTurn() {
        return playerTurn;
    }

    public void setRedPieces(List<RedPiece> redPieces) {
        this.redPieces = redPieces;
    }

    public void setBluePieces(List<BluePiece> bluePieces) {
        this.bluePieces = bluePieces;
    }

    public void setPlayerTurn(PlayerTurn playerTurn) {
        this.playerTurn = playerTurn;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
        .append(redPieces)
        .append(bluePieces)
        .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if(obj == null || !(obj instanceof MovesCore)) {
            return false;
        }

        MovesCore moves = (MovesCore) obj;

        return new EqualsBuilder().
                append(redPieces, moves.redPieces).
                append(bluePieces, moves.bluePieces).
                isEquals();
    }
}