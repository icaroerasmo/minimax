package br.com.iesb.table.moves.model;

import br.com.iesb.enums.Direction;

public class UserMoveModel {
    private int[] coordinates;
    private Direction[] directions;

    public UserMoveModel(int[] coordinates, Direction[] directions) {
        this.coordinates = coordinates;
        this.directions = directions;
    }

    public int[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(int[] coordinates) {
        this.coordinates = coordinates;
    }

    public Direction[] getDirections() {
        return directions;
    }

    public void setDirections(Direction[] direction) {
        this.directions = direction;
    }
}
