package br.com.iesb.table.moves.model;

import java.util.LinkedList;

/**
 * UserDTO
 */
public class PossibleMoves {

    private int captures;
    private LinkedList<MovesLog> movesLog;

    public LinkedList<MovesLog> getMovesLog() {
        return movesLog;
    }

    public void setMovesLog(LinkedList<MovesLog> movesLog) {
        this.movesLog = movesLog;
    }

    public int getCaptures() {
        return captures;
    }

    public void setCaptures(int captures) {
        this.captures = captures;
    }
}
