package br.com.iesb.table.moves;

import br.com.iesb.enums.Direction;
import br.com.iesb.enums.MoveType;
import br.com.iesb.table.TableState;
import br.com.iesb.table.moves.model.MovesLog;
import br.com.iesb.table.pieces.Piece;

public class UserMoves extends MovesAbstract{
    public UserMoves(MovesCore moves) {
        super(moves);
    }

    public TableState moveForwardLeft(Piece piece, TableState origin) {
        return move(piece, origin, Direction.FORWARD_LEFT);
    }

    public TableState moveForwardRight(Piece piece, TableState origin) {
        return move(piece, origin, Direction.FORWARD_RIGHT);
    }

    public TableState moveBackwardLeft(Piece piece, TableState origin) {
        return move(piece, origin, Direction.BACKWARD_LEFT);
    }

    public TableState moveBackwardRight(Piece piece, TableState origin) {
        return move(piece, origin, Direction.BACKWARD_RIGHT);
    }

    public TableState move(Piece piece, TableState origin, Direction direction) {

        TableState newState = origin.getCopy();
        Piece newPiece = newState.getPlayerPiece(piece.getLine(), piece.getColumn());

        int[] coordinates;

        switch(direction) {
            case FORWARD_LEFT -> coordinates = newPiece.testMoveForwardLeft();
            case FORWARD_RIGHT -> coordinates = newPiece.testMoveForwardRight();
            case BACKWARD_LEFT -> coordinates = newPiece.testMoveBackwardLeft();
            case BACKWARD_RIGHT -> coordinates = newPiece.testMoveBackwardRight();
            default -> throw new RuntimeException("Illegal value");
        }

        if (newState.theresPlayer(coordinates) ||
                !isMovementPermitted(coordinates) ||
                newState.theresPlayer(coordinates)) {
            throw new RuntimeException("Illegal movement");
        }

        if (theresOpponent(coordinates) && captured(newPiece, newState, direction)) {
            return newState;
        }

        int[] from = new int[]{newPiece.getLine(), newPiece.getColumn()};

        switch(direction) {
            case FORWARD_LEFT -> newPiece.moveForwardLeft();
            case FORWARD_RIGHT -> newPiece.moveForwardRight();
            case BACKWARD_LEFT -> newPiece.moveBackwardLeft();
            case BACKWARD_RIGHT -> newPiece.moveBackwardRight();
            default -> throw new RuntimeException("Illegal value");
        }

        int[] to = new int[]{newPiece.getLine(), newPiece.getColumn()};

        newState.addMessage(new MovesLog(from, to, newState.getPlayerTurn(), MoveType.MOVE, direction));

        return newState;
    }
}
