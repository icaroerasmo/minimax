package br.com.iesb.table.moves;

import br.com.iesb.enums.Direction;
import br.com.iesb.enums.MoveType;
import br.com.iesb.helpers.TableStateHelper;
import br.com.iesb.table.TableState;
import br.com.iesb.table.moves.model.MovesLog;
import br.com.iesb.table.pieces.BluePiece;
import br.com.iesb.table.pieces.Piece;
import br.com.iesb.table.pieces.RedPiece;

/**
 * MovesAbstract
 */
public abstract class MovesAbstract {

    private MovesCore moves;

    public MovesAbstract(MovesCore moves) {
        this.moves = moves;
    }

    public MovesCore getMoves() {
        return moves;
    }

    public boolean captured(Piece piece, TableState origin, Direction direction) {

        Piece generic;
        int[] coordinates, newCoordinates;

        switch(direction) {
            case FORWARD_LEFT -> {
                coordinates = piece.testMoveForwardLeft();
                generic = getNew(coordinates);
                newCoordinates = generic.testMoveForwardLeft();
            }
            case FORWARD_RIGHT -> {
                coordinates = piece.testMoveForwardRight();
                generic = getNew(coordinates);
                newCoordinates = generic.testMoveForwardRight();
            }
            case BACKWARD_LEFT -> {
                coordinates = piece.testMoveBackwardLeft();
                generic = getNew(coordinates);
                newCoordinates = generic.testMoveBackwardLeft();
            }
            case BACKWARD_RIGHT -> {
                coordinates = piece.testMoveBackwardRight();
                generic = getNew(coordinates);
                newCoordinates = generic.testMoveBackwardRight();
            }
            default -> throw new RuntimeException("Illegal value");
        }

        if (!isMovementPermitted(newCoordinates) ||
                origin.theresAnyPiece(newCoordinates)) {
            return false;
        }

        int[] from = new int[]{piece.getLine(), piece.getColumn()};

        switch(direction) {
            case FORWARD_LEFT -> {
                piece.moveForwardLeft();
                piece.moveForwardLeft();
            }
            case FORWARD_RIGHT -> {
                piece.moveForwardRight();
                piece.moveForwardRight();
            }
            case BACKWARD_LEFT -> {
                piece.moveBackwardLeft();
                piece.moveBackwardLeft();
            }
            case BACKWARD_RIGHT -> {
                piece.moveBackwardRight();
                piece.moveBackwardRight();
            }
            default -> throw new RuntimeException("Illegal value");
        }

        origin.removeOpponentPiece(coordinates[0], coordinates[1]);

        int[] to = new int[]{piece.getLine(), piece.getColumn()};

        origin.registerCapture();

        origin.addMessage(new MovesLog(from, coordinates, to, origin.getPlayerTurn(), MoveType.CAPTURE, direction));

        return true;
    }

    public boolean theresOpponentForwardLeft(Piece piece) {
        int[] coordinates = piece.testMoveForwardLeft();
        return theresOpponent(coordinates);
    }

    public boolean theresOpponentForwardRight(Piece piece) {
        int[] coordinates = piece.testMoveForwardRight();
        return theresOpponent(coordinates);
    }

    public boolean theresOpponentBackwardLeft(Piece piece) {
        int[] coordinates = piece.testMoveBackwardLeft();
        return theresOpponent(coordinates);
    }

    public boolean theresOpponentBackwardRight(Piece piece) {
        int[] coordinates = piece.testMoveBackwardRight();
        return theresOpponent(coordinates);
    }

    public boolean theresOpponent(int[] coordinates) {
        return getMoves().getPiece(
                getMoves().getOpponentPieces(),
                coordinates[0], coordinates[1]) != null;
    }

    public boolean theresPlayerForwardLeft(Piece piece) {
        int[] coordinates = piece.testMoveForwardLeft();
        return theresPlayer(coordinates);
    }

    public boolean theresPlayerForwardRight(Piece piece) {
        int[] coordinates = piece.testMoveBackwardRight();
        return theresPlayer(coordinates);
    }

    public boolean theresPlayerBackwardLeft(Piece piece) {
        int[] coordinates = piece.testMoveBackwardLeft();
        return theresPlayer(coordinates);
    }

    public boolean theresPlayerBackwardRight(Piece piece) {
        int[] coordinates = piece.testMoveBackwardRight();
        return theresPlayer(coordinates);
    }

    public boolean theresPlayer(int[] coordinates) {
        return getMoves().getPiece(
                getMoves().getPlayerPieces(),
                coordinates[0], coordinates[1]) != null;
    }

    public boolean theresAnyPiece(int[] coordinates) {
        return theresPlayer(coordinates) || theresOpponent(coordinates);
    }

    public Piece getNew(int[] coordinates) {
        return TableStateHelper.getNew(moves, coordinates);
    }

    public boolean isMovementPermitted(int[] coordinates) {
        return (coordinates[0] > -1) && (coordinates[0] < 8) &&
                (coordinates[1] > -1) && (coordinates[1] < 8);
    }
}
