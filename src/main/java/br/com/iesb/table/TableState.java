package br.com.iesb.table;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import br.com.iesb.enums.MinimaxDifficultyLevel;
import br.com.iesb.helpers.MovesHelper;
import br.com.iesb.helpers.StatesGeneratorHelper;
import br.com.iesb.helpers.TableStateHelper;
import br.com.iesb.mappers.PossibleMovesMapper;
import br.com.iesb.minimax.Heuristics;
import br.com.iesb.table.moves.*;
import br.com.iesb.table.moves.model.MovesLog;
import br.com.iesb.table.moves.model.PossibleMoves;
import br.com.iesb.table.moves.model.UserMoveModel;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import br.com.iesb.enums.PlayerTurn;
import br.com.iesb.table.pieces.Piece;
import org.mapstruct.factory.Mappers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TableState
 */
public class TableState {

	private static final Logger LOGGER = LoggerFactory.getLogger(TableState.class);
	private LinkedList<MovesLog> movesLog;
	private MovesCore movesCore;
	private MinimaxMoves minimaxMoves;
	private UserMoves userMoves;
	private MinimaxDifficultyLevel difficultyLevel = MinimaxDifficultyLevel.VERY_HARD;
	private int captures;

	public TableState() {
		this.captures = 0;
		this.movesCore = new MovesCore();
		this.minimaxMoves = new MinimaxMoves(movesCore);
		this.userMoves = new UserMoves(movesCore);
	}

	public TableState(MovesCore movesCore, int captures) {
		this.movesCore = movesCore;
		this.minimaxMoves = new MinimaxMoves(movesCore);
		this.userMoves = new UserMoves(movesCore);
		this.movesLog = new LinkedList<>();
		this.captures = captures;
	}

	public TableState(MovesCore movesCore) {
		this.movesCore = movesCore;
		this.minimaxMoves = new MinimaxMoves(movesCore);
		this.userMoves = new UserMoves(movesCore);
	}

	public boolean theresOpponentForwardLeft(Piece piece) {
		return minimaxMoves.theresOpponentForwardLeft(piece);
	}

	public boolean theresOpponentForwardRight(Piece piece) {
		return minimaxMoves.theresOpponentForwardRight(piece);
	}

	public boolean theresOpponentBackwardLeft(Piece piece) {
		return minimaxMoves.theresOpponentBackwardLeft(piece);
	}

	public boolean theresOpponentBackwardRight(Piece piece) {
		return minimaxMoves.theresOpponentBackwardRight(piece);
	}

	public boolean theresPlayer(int[] coordinates) {
		return minimaxMoves.theresPlayer(coordinates);
	}

	public boolean theresAnyPiece(int[] coordinates) {
		return minimaxMoves.theresAnyPiece(coordinates);
	}

	public List<TableState> minimaxForwardLeft(int line, int column) {
		return minimaxMoves.moveForwardLeft(getPlayerPiece(line, column), this);
	}

	public List<TableState> minimaxForwardRight(int line, int column) {
		return minimaxMoves.moveForwardRight(getPlayerPiece(line, column), this);
	}

	public List<TableState> minimaxBackwardLeft(int line, int column) {
		return minimaxMoves.moveBackwardLeft(getPlayerPiece(line, column), this);
	}

	public List<TableState> minimaxBackwardRight(int line, int column) {
		return minimaxMoves.moveBackwardRight(getPlayerPiece(line, column), this);
	}

	public TableState userForwardLeft(int line, int column) {
		return userMoves.moveForwardLeft(getPlayerPiece(line, column), this);
	}

	public TableState userForwardRight(int line, int column) {
		return userMoves.moveForwardRight(getPlayerPiece(line, column), this);
	}

	public TableState userBackwardLeft(int line, int column) {
		return userMoves.moveBackwardLeft(getPlayerPiece(line, column), this);
	}

	public TableState userBackwardRight(int line, int column) {
		return userMoves.moveBackwardRight(getPlayerPiece(line, column), this);
	}

	public void registerCapture() {
		captures += 1;
	}

	public int getCaptures() {
		return captures;
	}

	public Piece getRedPiece(int line, int column) {
		return movesCore.getRedPiece(line, column);
	}

	public List<? extends Piece> getRedPieces() {
		return movesCore.getRedPieces();
	}

	public List<? extends Piece> getBluePieces() {
		return movesCore.getBluePieces();
	}

	public Piece getBluePiece(int line, int column) {
		return movesCore.getBluePiece(line, column);
	}

	public Piece getPlayerPiece(int line, int column) {
		return movesCore.getPlayerPiece(line, column);
	}

	public List<? extends Piece> getPlayerPieces() {
		return movesCore.getPlayerPieces();
	}

	public List<? extends Piece> getOpponentPieces() {
		return movesCore.getOpponentPieces();
	}

	public Piece getOpponentPiece(int line, int column) {
		return movesCore.getOpponentPiece(line, column);
	}

	public void removeOpponentPiece(int line, int column) {
		movesCore.removeOpponentPiece(line, column);
	}

	public PlayerTurn getPlayerTurn() {
		return movesCore.getPlayerTurn();
	}

	public boolean isRedTurn() {
		return movesCore.isRedTurn();
	}

	public boolean isBlueTurn() {
		return movesCore.isBlueTurn();
	}

	public void setPlayerTurn(PlayerTurn playerTurn) {
		movesCore.setPlayerTurn(playerTurn);
	}

	public boolean isFinished() {
		return movesCore.getBluePieces().size() < 1 || movesCore.getRedPieces().size() < 1;
	}

	public String printWinner() {
		return TableStateHelper.printWinner(this);
	}

	public TableState doUserMove(UserMoveModel moves) {
		return MovesHelper.doUserMove(moves, this);
	}

	public TableState doMinimaxMove() {
		return MovesHelper.doMinimaxMove(difficultyLevel,this);
	}

	public List<PossibleMoves> getPossibleMovesForUser(int line, int column) {
		PossibleMovesMapper possibleMovesMapper = Mappers.getMapper(PossibleMovesMapper.class);
		return possibleMovesMapper.tableToUserDTO(StatesGeneratorHelper.
				getMovesForSinglePiece(getPlayerPiece(line, column), this, false));
	}

	public void changePlayersTurn() {
		movesCore.changePlayersTurn();
	}

	public List<MovesLog> getMovesLog() {
		return movesLog;
	}

	public void clearLogs() {
		this.movesLog.clear();
	}

	public TableState getCopy() {
		return TableStateHelper.getCopy(this);
	}

	public Piece[][] getTable() {
		return TableStateHelper.getTable(this);
	}

	public MinimaxDifficultyLevel getDifficultyLevel() {
		return difficultyLevel;
	}

	public void setDifficultyLevel(MinimaxDifficultyLevel difficultyLevel) {
		this.difficultyLevel = difficultyLevel;
	}

	public void printAllLogs() {
		TableStateHelper.printAllLogs(this);
	}

	public void printUnreadLogs() {
		TableStateHelper.printUnreadLogs(this);
	}

	public void addMessage(MovesLog message) {
		TableStateHelper.addMessage(this, message);
	}

	public MinimaxMoves getMinimaxMoves() {
		return minimaxMoves;
	}

	public int getScore() {
		return Heuristics.calc(this);
	}

	public MovesCore getMovesCore() {
		return movesCore;
	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();

		Piece[][] table = this.getTable();

		for(var line = 7; line > -1; line--) {
			builder.append("\033[0m"+ line + "   ");
			for(var column = 0; column < 8; column++) {
				var piece = table[line][column];
				if (piece != null) {
					final String pieceType = piece.getIsChecker() ? "A " : "0 ";
					builder.append(piece.getColor() + pieceType);
				} else {
					builder.append("\033[0m"+ "- ");
				}
			}
			builder.append(System.lineSeparator());
		}
		builder.append(System.lineSeparator());
		builder.append("\033[0m"+"    0 1 2 3 4 5 6 7");

		return builder.toString();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(
				17,
				37).
				append(movesCore).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {

		if(obj == null || !(obj instanceof TableState)) {
			return false;
		}

		TableState matrix = (TableState) obj;

		return new EqualsBuilder().
				append(movesCore, matrix.movesCore).
				isEquals();
	}
}