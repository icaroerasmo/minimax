package br.com.iesb.table.pieces;

/**
 * RedPiece
 */
public class RedPiece extends Piece {

    public RedPiece() {
        super();
    }

    public RedPiece(int line, int column) {
        super(line, column);
    }
    
    public RedPiece(int line, int column, boolean isChecker) {
        super(line, column, isChecker);
    }

    public RedPiece(int[] coordinates) {
        super(coordinates);
	}

    @Override
    public void moveForward() {
        changeLine(incrementLine());
    }

    @Override
    public void moveBackward() {
        changeLine(decrementLine());
    }

    @Override
    public void moveLeft() {
        changeColumn(decrementColumn());
    }

    @Override
    public void moveRight() {
        changeColumn(incrementColumn());
    }
	@Override
    public boolean moveForwardLeft(){
        boolean mov;
        if(mov = canMove(tryMoveForward(), tryMoveLeft())){
            moveForward();
            moveLeft();
            turnoIntoChecker();
        }
        return mov;
    }

    @Override
    public boolean moveForwardRight(){
        boolean mov;
        if(mov = canMove(tryMoveForward(), tryMoveRight())){
            moveForward();
            moveRight();
            turnoIntoChecker();
        }
        return mov;
    }

    @Override
    public boolean moveBackwardLeft() {
        boolean mov;
        if(mov = canMove(tryMoveBackward(), tryMoveLeft())){
            moveBackward();
            moveLeft();
        }
        return mov;
    }

    @Override
    public boolean moveBackwardRight() {
        boolean mov;
        if(mov = canMove(tryMoveBackward(), tryMoveRight())){
            moveBackward();
            moveRight();
        }
        return mov;
    }

    @Override
    public String getColor() {
        return "\033[031m";
    }

    @Override
    public boolean tryMoveForward() {
        return tryIncrementLine();
    }

    @Override
    public boolean tryMoveBackward() {
        return tryDecrementLine();
    }

    @Override
    public boolean tryMoveLeft() {
        return tryDecrementColumn();
    }

    @Override
    public boolean tryMoveRight() {
        return tryIncrementColumn();
    }

    @Override
    public int testMoveForward() {
        return incrementLine();
    }

    @Override
    public int testMoveBackward() {
        return decrementLine();
    }

    @Override
    public int testMoveLeft() {
        return decrementColumn();
    }

    @Override
    public int testMoveRight() {
        return incrementColumn();
    }

    @Override
    public int[] testMoveForwardLeft(){
        return new int[] {testMoveForward(), testMoveLeft()};
    }

    @Override
    public int[] testMoveForwardRight(){
        return new int[] {testMoveForward(), testMoveRight()};
    }
    
    @Override
    public int[] testMoveBackwardLeft(){
        return new int[] {testMoveBackward(), testMoveLeft()};
    }

    @Override
    public int[] testMoveBackwardRight(){
        return new int[] {testMoveBackward(), testMoveRight()};
    }

    @Override
    public boolean turnoIntoChecker() {
        if(getLine() == 7){
            return super.turnoIntoChecker();
        }
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof RedPiece)) {
            return false;
        }
        return super.equals(obj);
    }
}