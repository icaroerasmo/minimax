package br.com.iesb.table.pieces;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Piece
 */
public abstract class Piece {
    
    private int line;
    private int column;
    private boolean isChecker;

    public Piece() {}

    public Piece(int line, int column) {
    	this.line = line;
        this.column = column;
        this.isChecker = false;
    }
    
    public Piece(int line, int column, boolean isChecker) {
    	this.line = line;
        this.column = column;
        this.isChecker = isChecker;
    }

    public Piece(int[] coordinates) {
        this(coordinates[0], coordinates[1]);
    }

    public int getColumn() {
        return column;
    }

    public int getLine() {
        return line;
    }

    public boolean getIsChecker() {
        return isChecker;
    }

    public void setIsChecker(boolean isChecker) {
        this.isChecker = isChecker;
    }

    public boolean turnoIntoChecker(){
        return isChecker = true;
    }

    protected int incrementLine() {
        return line + 1;
    }

    protected int decrementLine() {
        return line - 1;
    }

    protected int incrementColumn() {
        return column + 1;
    }

    protected int decrementColumn() {
        return column - 1;
    }

    protected boolean tryIncrementColumn() {
        return column < 7;
    }

    protected boolean tryDecrementColumn() {
        return column > 0;
    }

    protected boolean tryIncrementLine() {
        return line < 7;
    }

    protected boolean tryDecrementLine() {
        return line > 0;
    }

    protected void changeLine(int line) {
        int diff = Math.abs(this.line - line);
        if(diff <= 1) {
            this.line = line;
        } else {
            throw new RuntimeException("Move a piece more than one step at time is not allowed.");
        }
    }

    protected void changeColumn(int column) {
        int diff = Math.abs(this.column - column);
        if(diff <= 1) {
            this.column = column;
        } else {
            throw new RuntimeException("Move a piece more than one step at time is not allowed.");
        }
    }

    public void setLine(int line) {
        this.line = line;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public boolean canMove(boolean canMoveX, boolean canMoveY) {
        return canMoveX && canMoveY;
    }

    public abstract boolean tryMoveForward();
    public abstract boolean tryMoveBackward();
    public abstract boolean tryMoveLeft();
    public abstract boolean tryMoveRight();
    public abstract int testMoveForward();
    public abstract int testMoveBackward();
    public abstract int testMoveLeft();
    public abstract int testMoveRight();
    public abstract int[] testMoveForwardLeft();
    public abstract int[] testMoveForwardRight();
    public abstract int[] testMoveBackwardLeft();
    public abstract int[] testMoveBackwardRight();

    public abstract void moveForward();
    public abstract void moveBackward();
    public abstract void moveLeft();
    public abstract void moveRight();
    public abstract boolean moveForwardLeft();
    public abstract boolean moveForwardRight();
    public abstract boolean moveBackwardLeft();
    public abstract boolean moveBackwardRight();
    public abstract String getColor();
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
        .append(line)
        .append(column)
        .append(isChecker)
        .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if(obj == null || !(obj instanceof Piece)) {
            return false;
        }

        Piece piece = (Piece) obj;

        EqualsBuilder equalsBuilder = new EqualsBuilder();

        return equalsBuilder.
                append(line, piece.line).
                append(column, piece.column).
                append(isChecker, piece.isChecker).
                isEquals();
    }
}