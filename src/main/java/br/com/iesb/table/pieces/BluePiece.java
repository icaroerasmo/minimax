package br.com.iesb.table.pieces;

/**
 * BluePiece
 */
public class BluePiece extends Piece {

    public BluePiece() {
        super();
    }

    public BluePiece(int line, int column) {
        super(line, column);
    }
    
    public BluePiece(int line, int column, boolean isChecker) {
        super(line, column, isChecker);
    }

    public BluePiece(int[] coordinates) {
        super(coordinates);
    }

    @Override
    public void moveForward() {
        this.changeLine(decrementLine());
    }

    @Override
    public void moveBackward() {
        this.changeLine(incrementLine());
    }

    @Override
    public void moveLeft() {
        this.changeColumn(incrementColumn());
    }

    @Override
    public void moveRight() {
        this.changeColumn(decrementColumn());
    }
    @Override
	public boolean moveForwardLeft(){
        boolean mov;
        if(mov = canMove(tryMoveForward(), tryMoveLeft())) {
            moveForward();
            moveLeft();
            turnoIntoChecker();
        }
        return mov;
    }

    @Override
    public boolean moveForwardRight(){
        boolean mov;
        if(mov = canMove(tryMoveForward(), tryMoveRight())){
            moveForward();
            moveRight();
            turnoIntoChecker();
        }
        return mov;
    }
    
    @Override
    public boolean moveBackwardLeft() {
        boolean mov;
        if(mov = canMove(tryMoveBackward(), tryMoveLeft())){
            moveBackward();
            moveLeft();
        }
        return mov;
    }

    @Override
    public boolean moveBackwardRight() {
        boolean mov;
        if(mov = canMove(tryMoveBackward(), tryMoveRight())){
            moveBackward();
            moveRight();
        }
        return mov;
    }

    @Override
    public String getColor() {
        return "\033[034m";
    }

    @Override
    public boolean tryMoveForward() {
        return tryDecrementLine();
    }

    @Override
    public boolean tryMoveBackward() {
        return tryIncrementLine();
    }

    @Override
    public boolean tryMoveLeft() {
        return tryIncrementColumn();
    }

    @Override
    public boolean tryMoveRight() {
        return tryDecrementColumn();
    }

    @Override
    public int testMoveForward() {
        return decrementLine();
    }

    @Override
    public int testMoveBackward() {
        return incrementLine();
    }

    @Override
    public int testMoveLeft() {
        return incrementColumn();
    }

    @Override
    public int testMoveRight() {
        return decrementColumn();
    }

    @Override
    public int[] testMoveForwardLeft(){
        return new int[] {testMoveForward(), testMoveLeft()};
    }

    @Override
    public int[] testMoveForwardRight(){
        return new int[] {testMoveForward(), testMoveRight()};
    }
    
    @Override
    public int[] testMoveBackwardLeft(){
        return new int[] {testMoveBackward(), testMoveLeft()};
    }

    @Override
    public int[] testMoveBackwardRight(){
        return new int[] {testMoveBackward(), testMoveRight()};
    }

    @Override
    public boolean turnoIntoChecker() {
        if(getLine() == 0){
            return super.turnoIntoChecker();
        }
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof BluePiece)) {
            return false;
        }
        return super.equals(obj);
    }
}