package br.com.iesb.table.dtos;

import br.com.iesb.table.pieces.BluePiece;
import br.com.iesb.table.pieces.RedPiece;

import java.util.List;

public class TablePiecesDTO {

    private List<RedPiece> redPieces;
    private List<BluePiece> bluePieces;

    public List<RedPiece> getRedPieces() {
        return redPieces;
    }
    public void setRedPieces(List<RedPiece> redPieces) {
        this.redPieces = redPieces;
    }
    public List<BluePiece> getBluePieces() {
        return bluePieces;
    }
    public void setBluePieces(List<BluePiece> bluePieces) {
        this.bluePieces = bluePieces;
    }
}
