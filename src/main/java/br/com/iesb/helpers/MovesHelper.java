package br.com.iesb.helpers;

import br.com.iesb.enums.Direction;
import br.com.iesb.enums.MinimaxDifficultyLevel;
import br.com.iesb.enums.PlayerTurn;
import br.com.iesb.minimax.MinimaxRuntime;
import br.com.iesb.table.TableState;
import br.com.iesb.table.moves.model.UserMoveModel;
import br.com.iesb.table.pieces.Piece;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Queue;

public class MovesHelper {

    final static Logger LOGGER = LoggerFactory.getLogger(MovesHelper.class);

    public static TableState doMinimaxMove(MinimaxDifficultyLevel difficulty, TableState origin) {
        int currentScore = 0;
        TableState currentTB = null;

        TableStateHelper.logTurn(origin);

        List<TableState> possibleMovements = StatesGeneratorHelper.getPossibleMovements(origin);

        LOGGER.info("{} possible moves found this turn", possibleMovements.size());

        for (TableState matrix : possibleMovements) {
            var score = MinimaxRuntime.minimax(matrix, difficulty.getLevel(), false);
            if (currentTB == null || currentScore < score) {
                currentScore = score;
                currentTB = matrix;
            }
        }

        if(currentTB != null) {
            currentTB.setPlayerTurn(origin.getPlayerTurn());
        }

        return currentTB;
    }

    public static TableState doUserMove(UserMoveModel moves, TableState currentUserState) {

        final Direction[] directions = moves.getDirections();

        int movesCounter = 0;
        int[] coordinates = moves.getCoordinates();

        Piece playerPiece = currentUserState.getPlayerPiece(coordinates[0], coordinates[1]);

        Direction direction = null;
        TableState previousState;

        do{

            previousState = currentUserState;
            direction = directions[movesCounter++];

            int[] destinationCoordinates = null;

            switch (direction) {
                case FORWARD_LEFT -> {
                    destinationCoordinates = playerPiece.testMoveForwardLeft();
                    currentUserState = currentUserState.userForwardLeft(coordinates[0], coordinates[1]);
                }
                case FORWARD_RIGHT -> {
                    destinationCoordinates = playerPiece.testMoveForwardRight();
                    currentUserState = currentUserState.userForwardRight(coordinates[0], coordinates[1]);
                }
                case BACKWARD_LEFT -> {
                    destinationCoordinates = playerPiece.testMoveBackwardLeft();
                    currentUserState = currentUserState.userBackwardLeft(coordinates[0], coordinates[1]);
                }
                case BACKWARD_RIGHT -> {
                    destinationCoordinates = playerPiece.testMoveBackwardRight();
                    currentUserState = currentUserState.userBackwardRight(coordinates[0], coordinates[1]);
                }
            }

            if(!playerHasCaptured(currentUserState, previousState)) {
                coordinates = destinationCoordinates;
                playerPiece = currentUserState.getPlayerPiece(coordinates[0], coordinates[1]);
            } else {
                switch (direction) {
                    case FORWARD_LEFT -> {
                        coordinates = TableStateHelper.getNew(currentUserState, destinationCoordinates).testMoveForwardLeft();
                    }
                    case FORWARD_RIGHT -> {
                        coordinates = TableStateHelper.getNew(currentUserState, destinationCoordinates).testMoveForwardRight();
                    }
                    case BACKWARD_LEFT -> {
                        coordinates = TableStateHelper.getNew(currentUserState, destinationCoordinates).testMoveBackwardLeft();
                    }
                    case BACKWARD_RIGHT -> {
                        coordinates = TableStateHelper.getNew(currentUserState, destinationCoordinates).testMoveBackwardRight();
                    }
                }
                playerPiece = currentUserState.getPlayerPiece(coordinates[0], coordinates[1]);
            }

        } while(canMove(currentUserState, previousState, directions, playerPiece, movesCounter));

        LOGGER.info("\n" + currentUserState);
        currentUserState.printUnreadLogs();

        return currentUserState;
    }

    private static boolean canMove(
            TableState currentUserState,  TableState previousState,
            Direction[] directions, Piece playerPiece, int movesCounter) {

        if(!thereAreMovesLeft(directions.length, movesCounter)) {
            return false;
        }

        final Direction
                direction = directions[movesCounter],
                previousDirection = directions[movesCounter-1];

        return (!playerHasCaptured(currentUserState, previousState) && isCheckerMove(playerPiece, previousDirection, direction)) ||
                (playerHasCaptured(currentUserState, previousState) && canCaptureMore(currentUserState, direction, playerPiece));
    }

    private static boolean canCaptureMore(TableState currentUserState, Direction direction, Piece playerPiece) {
        switch (direction) {
            case FORWARD_LEFT -> {
                final Piece generic = TableStateHelper.getNew(currentUserState, playerPiece.testMoveForwardLeft());
                return currentUserState.theresOpponentForwardLeft(playerPiece) &&
                        !currentUserState.theresAnyPiece(generic.testMoveForwardLeft());
            }
            case FORWARD_RIGHT -> {
                final Piece generic = TableStateHelper.getNew(currentUserState, playerPiece.testMoveForwardRight());
                return currentUserState.theresOpponentForwardRight(playerPiece) &&
                        !currentUserState.theresAnyPiece(generic.testMoveForwardRight());
            }
            case BACKWARD_LEFT -> {
                final Piece generic = TableStateHelper.getNew(currentUserState, playerPiece.testMoveBackwardLeft());
                return currentUserState.theresOpponentBackwardLeft(playerPiece) &&
                        !currentUserState.theresAnyPiece(generic.testMoveBackwardLeft());
            }
            case BACKWARD_RIGHT -> {
                final Piece generic = TableStateHelper.getNew(currentUserState, playerPiece.testMoveBackwardRight());
                return currentUserState.theresOpponentBackwardRight(playerPiece) &&
                        !currentUserState.theresAnyPiece(generic.testMoveBackwardRight());
            }
            default -> {
                return false;
            }
        }
    }

    private static boolean playerHasCaptured(TableState currentUserState, TableState previousState) {
        return !currentUserState.getOpponentPieces().
                equals(previousState.getOpponentPieces());
    }

    private static boolean thereAreMovesLeft(int directionsCounter, int movesCounter) {
        return movesCounter < directionsCounter;
    }

    private static boolean isCheckerMove(Piece playerPiece, Direction previousDirection, Direction direction) {
        return playerPiece.getIsChecker() && (previousDirection == null || direction.equals(previousDirection));
    }
}
