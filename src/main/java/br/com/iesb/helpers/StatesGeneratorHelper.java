package br.com.iesb.helpers;

import br.com.iesb.table.TableState;
import br.com.iesb.table.pieces.Piece;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * StatesGeneratorHelper
 */
public class StatesGeneratorHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(StatesGeneratorHelper.class);

    public static List<TableState> getPossibleMovements(final TableState matrix) {

        List<? extends Piece> pieces = matrix.getPlayerPieces();

        List<TableState> matrixes = pieces.stream().
                flatMap(p -> StatesGeneratorHelper.
                        getMovesForSinglePiece(p, matrix).stream())
                .collect(Collectors.toList());

        matrixes.forEach(mtrx -> mtrx.changePlayersTurn());

        return matrixes;
    }

    public static List<TableState> getMovesForSinglePiece(Piece p, TableState matrix) {
        return getMovesForSinglePiece(p, matrix, true);
    }

    public static List<TableState> getMovesForSinglePiece(Piece p, TableState matrix, boolean keepLogs) {

        var forwardLeft = matrix.getCopy();
        var forwardRight = matrix.getCopy();
        var backwardLeft = matrix.getCopy();
        var backwardRight = matrix.getCopy();

        if(!keepLogs) {
            forwardLeft.clearLogs();
            forwardRight.clearLogs();
            backwardLeft.clearLogs();
            backwardRight.clearLogs();
        }

        return Stream.of(
                    forwardLeft.minimaxForwardLeft(p.getLine(), p.getColumn()),
                    forwardRight.minimaxForwardRight(p.getLine(), p.getColumn()),
                    backwardLeft.minimaxBackwardLeft(p.getLine(), p.getColumn()),
                    backwardRight.minimaxBackwardRight(p.getLine(), p.getColumn())
                ).flatMap(states -> states.stream()).collect(Collectors.toList());
    }
}
