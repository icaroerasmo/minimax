package br.com.iesb.helpers;

import br.com.iesb.enums.PlayerTurn;
import br.com.iesb.table.TableState;
import br.com.iesb.table.moves.MovesCore;
import br.com.iesb.table.moves.model.MovesLog;
import br.com.iesb.table.pieces.BluePiece;
import br.com.iesb.table.pieces.Piece;
import br.com.iesb.table.pieces.RedPiece;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * TableStateHelper
 */
public class TableStateHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(TableStateHelper.class);

    public static void logTurn(TableState matrix) {

        var reds = matrix.getRedPieces().size();
        var blues = matrix.getBluePieces().size();

        final String msg = "{}'s turn - {}: {}  {}: {}";

        LOGGER.info(msg, matrix.getPlayerTurn(), PlayerTurn.RED, reds, PlayerTurn.BLUE, blues);
    }

    public static Piece[][] getTable(TableState origin) {

        Piece[][] matrix = new Piece[8][8];

        Stream.concat(
                        origin.getRedPieces().stream(),
                        origin.getBluePieces().stream()).
                forEach(p -> matrix[p.getLine()][p.getColumn()] = p);

        return matrix;
    }

    public static TableState getCopy(TableState origin) {
        List<RedPiece> reds = new ArrayList<>();
        List<BluePiece> blues = new ArrayList<>();

        origin.getRedPieces().forEach(r -> reds.add(new RedPiece(r.getLine(), r.getColumn(), r.getIsChecker())));
        origin.getBluePieces().forEach(b -> blues.add(new BluePiece(b.getLine(), b.getColumn(), b.getIsChecker())));

        MovesCore moves = new MovesCore(reds, blues, origin.getPlayerTurn());

        var next = new TableState(moves, origin.getCaptures());

        if(origin.getMovesLog() != null) {
            next.getMovesLog().addAll(origin.getMovesLog());
        }

        return next;
    }

    public static String printWinner(TableState matrix) {

        var redPieces = matrix.getRedPieces();
        var bluePieces = matrix.getBluePieces();

        if(redPieces.size() > bluePieces.size()) {
            return "Red won";
        } else if(bluePieces.size() > redPieces.size()) {
            return "Blue won";
        } else {
            return "Draw";
        }
    }

    public static void addMessage(TableState matrix, MovesLog message) {
        var movesLog = matrix.getMovesLog();
        if(movesLog != null) {
            movesLog.add(message);
        }
    }

    public static Piece getNew(TableState tableState, int[] coordinates) {
        return getNew(tableState.getMovesCore(), coordinates);
    }

    public static Piece getNew(MovesCore movesCore, int[] coordinates) {
        return movesCore.isRedTurn() ? new RedPiece(coordinates) : new BluePiece(coordinates);
    }

    public static void printAllLogs(TableState origin) {
        if(origin.getMovesLog() == null) {
            return;
        }
        printLogs(origin.getMovesLog().stream());
    }

    public static void printUnreadLogs(TableState origin) {
        if(origin.getMovesLog() == null) {
            return;
        }
        printLogs(origin.getMovesLog().stream().filter(MovesLog::isNotRead));
    }

    public static void printLogs(Stream<MovesLog> movesLogs) {
        movesLogs.forEach(move -> LOGGER.info(move.toString()));
    }
}
