package br.com.iesb.mappers;

import br.com.iesb.table.moves.model.PossibleMoves;
import br.com.iesb.table.TableState;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

/**
 * UserMapper
 */
@Mapper
public interface PossibleMovesMapper {

    PossibleMoves tableToUserDTO(TableState matrix);
    List<PossibleMoves> tableToUserDTO(List<TableState> matrixes);
}
