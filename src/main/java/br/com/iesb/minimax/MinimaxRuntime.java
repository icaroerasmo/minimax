package br.com.iesb.minimax;

import br.com.iesb.table.TableState;
import br.com.iesb.helpers.StatesGeneratorHelper;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * MinimaxRuntime
 */
public class MinimaxRuntime {

	public static int minimax(TableState matrix, int depth, Boolean isMax) {
		if(matrix.isFinished() || depth == 0) {
			return matrix.getScore();
		} else {
			List<TableState> possibleMovements = StatesGeneratorHelper.getPossibleMovements(matrix);
			if(!isMax) {
				Optional<TableState> child = possibleMovements.
						stream().min(Comparator.comparingInt(TableState::getScore));

				if(child.isEmpty()) {
					return matrix.getScore();
				}

				return min(matrix.getScore(), minimax(child.get(), depth-1, !isMax));
			} else {
				Optional<TableState> child = possibleMovements.
						stream().max(Comparator.comparingInt(TableState::getScore));

				if(child.isEmpty()) {
					return matrix.getScore();
				}

				return max(matrix.getScore(), minimax(child.get(), depth-1, !isMax));
			}
		}
	}

	public static int min(int val1, int val2) {
		return val1 > val2 ? val2 : val1;
	}

	public static int max(int val1, int val2) {
		return val2 > val1 ? val2 : val1;
	}
}
