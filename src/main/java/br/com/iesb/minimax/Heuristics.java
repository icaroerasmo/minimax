package br.com.iesb.minimax;

import java.util.Comparator;
import java.util.List;

import br.com.iesb.helpers.MovesHelper;
import br.com.iesb.helpers.StatesGeneratorHelper;
import br.com.iesb.table.TableState;
import br.com.iesb.table.moves.model.PossibleMoves;
import br.com.iesb.table.pieces.Piece;

/**
 * Heuristics
 */
public class Heuristics {

	private static final int[][] HEURISTIC = new int[][]{
			{4, 4, 4, 4, 4, 4, 4, 4},
			{4, 3, 3, 3, 3, 3, 3, 4},
			{4, 3, 2, 2, 2, 2, 3, 4},
			{4, 3, 2, 1, 1, 2, 3, 4},
			{4, 3, 2, 1, 1, 2, 3, 4},
			{4, 3, 2, 2, 2, 2, 3, 4},
			{4, 3, 3, 3, 3, 3, 3, 4},
			{4, 4, 4, 4, 4, 4, 4, 4}
	};

	// TODO fix heuristic
	public static int calc(TableState matrix) {
		return matrix.getCaptures() > 0 ? (3 * matrix.getCaptures()) : avg(matrix.getPlayerPieces());
	}

	private static int sumPieces(List<? extends Piece> pieces) {
		var score = 0;
		for(Piece p : pieces) {
			score += HEURISTIC[p.getLine()][p.getColumn()];
		}
		return score;
	}

	private static int avg(List<? extends Piece> pieces) {
		return pieces.size() > 0 ? sumPieces(pieces) / pieces.size() : 0;
	}
}
