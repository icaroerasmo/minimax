package br.com.iesb.enums;

import java.util.stream.Stream;

/**
 * PlayerTurn
 */
public enum PlayerTurn {
    RED("Red"), BLUE("Blue");

    private String description;

    PlayerTurn(String description) {
        this.description = description;
    }

    public PlayerTurn changeTurn() {
        return Stream.of(values()).filter(
                p -> !this.equals(p)).
                findFirst().orElseThrow(
                        () -> new RuntimeException("Turn not found"));
    }

    @Override
    public String toString() {
        return description;
    }
}