package br.com.iesb.enums;

/**
 * Direction
 */
public enum Direction {

    FORWARD_LEFT("forward left", false),
    FORWARD_RIGHT("forward right", false),
    BACKWARD_LEFT("backward left", true),
    BACKWARD_RIGHT("backward right", true);

    private String description;
    private Boolean mustBeChecker;

    Direction(String description, Boolean mustBeChecker) {
        this.description = description;
        this.mustBeChecker = mustBeChecker;
    }

    public Boolean getMustBeChecker() {
        return mustBeChecker;
    }

    @Override
    public String toString() {
        return description;
    }
}
