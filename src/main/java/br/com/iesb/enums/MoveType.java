package br.com.iesb.enums;

/**
 * MoveType
 */
public enum MoveType {
    MOVE("moved"), CAPTURE("captured");
    private String description;
    MoveType(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }
}
