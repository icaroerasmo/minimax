package br.com.iesb.enums;

public enum MinimaxDifficultyLevel {
    EASY(5), NORMAL(15), HARD(20), VERY_HARD(25);

    private Integer level;

    MinimaxDifficultyLevel(Integer level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }
}
