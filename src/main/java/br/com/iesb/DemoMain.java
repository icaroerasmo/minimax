package br.com.iesb;

import br.com.iesb.table.TableState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * DemoMain
 */
public class DemoMain {

	private static final Logger LOGGER = LoggerFactory.getLogger(DemoMain.class);

	private static final int MAX_TURNS = 100;

	public static void main(String[] args) {
		var matrix = new TableState();
		int turns = MAX_TURNS;

		LOGGER.info("Starting a new game");
		LOGGER.info(System.lineSeparator() + matrix);

		while(!matrix.isFinished() && --turns > 0) {

			LOGGER.info("Turn: {}", (MAX_TURNS - turns));

			var nextMatrix = matrix.doMinimaxMove();

			if(nextMatrix == null) {
				break;
			}

			nextMatrix.changePlayersTurn();

			matrix = nextMatrix;

			LOGGER.info(System.lineSeparator() + matrix);

			matrix.printUnreadLogs();
		}
		LOGGER.info("Game is over: {}", matrix.printWinner());
	}
}